import { getPictureGroup } from '@/api/picture'

export default {
  data() {
    return {
      picGroup: this.$store.state.picture.picGroup,//相册列表
    }
  },
  mounted() {
    this.getPicGroup();//获取照片分组
  },
  methods: {
    /**
     * 获取相册分组
     */
    getPicGroup() {
      const params = { key: this.$store.state.app.activeApp.saa_key, status: '1', limit: 100 }
      getPictureGroup(params).then( response => {
        if(response.status === 200){
          this.picGroup = response.data;
          this.picGroup.forEach(element => {
            if(element.status === '1'){
              element.status = true;
            }else{
              element.status = false;
            }
          });
          this.$store.dispatch('picture/PicGroup',this.picGroup)
        }else{
          this.picGroup = []
        this.$store.dispatch('picture/PicGroup',this.picGroup)
          this.$message(response.message);
        }
      } )
    },
  },
}
